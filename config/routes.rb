Rails.application.routes.draw do
  resources :projects
  get 'static_pages/about'
  root 'static_pages#about'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
